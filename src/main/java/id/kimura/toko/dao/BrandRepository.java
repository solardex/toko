package id.kimura.toko.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import id.kimura.toko.model.Brand;
import id.kimura.toko.pojo.Ja;
import id.kimura.toko.pojo.Select2;

public interface BrandRepository extends JpaRepository<Brand, Long> {

    @Query("SELECT b FROM Brand b WHERE b.name LIKE %?1%"
        + " OR b.code LIKE %?1%"
    )
    public List<Brand> ac(String q); // ac = autocomplete

    @Query(value="SELECT id, code AS value, name AS label "
        + " FROM Brand WHERE name LIKE %?1% "
        + " OR code LIKE %?1% "
    , nativeQuery = true)
    public List<Object> jaNative(String q); // ja = jquery autocomplete

    @Query(
        "SELECT new id.kimura.toko.pojo.Ja(b.id, b.code, b.code || ' - ' || b.name)  "
        + " FROM Brand b WHERE b.name LIKE %?1% "
        + " OR b.code LIKE %?1% "
    )
    public List<Ja> ja(String q); // ja = jquery autocomplete

    @Query(
        "SELECT new id.kimura.toko.pojo.JaCustom(b.id, b.code, b.code || ' - ' || b.name, b.name)  "
        + " FROM Brand b WHERE b.name LIKE %?1% "
        + " OR b.code LIKE %?1% "
    )
    public List<Ja> jaCustom(String q); // ja = jquery autocomplete

    @Query(
        "SELECT new id.kimura.toko.pojo.Select2(b.id, b.code)  "
        + " FROM Brand b WHERE b.name LIKE %?1% "
        + " OR b.code LIKE %?1% "
    )
    public List<Select2> s2(String q); 

}

