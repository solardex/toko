package id.kimura.toko.dao;

// import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import id.kimura.toko.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {


}
