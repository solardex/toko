package id.kimura.toko.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.web.bind.annotation.GetMapping;
// import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import id.kimura.toko.model.Customer;
import id.kimura.toko.dao.CustomerRepository;

@Controller
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    CustomerRepository customerRepository;

    @GetMapping("/create")
    public String create(Customer cust) {
        return "customer/create";
    }

    @PostMapping("/save")
    public String save(@Valid Customer cust, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "customer/create";
        }
        
        customerRepository.save(cust);
        return "redirect:/customer/index";
    }

    @GetMapping("/index")
    public String index(Model model) {
        model.addAttribute("customers", customerRepository.findAll());
        return "customer/index";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") long id, Model model) {
        Customer cust = customerRepository.findById(id)
          .orElseThrow(() -> new IllegalArgumentException("Invalid customer Id:" + id));
        
        model.addAttribute("cust", cust);
        return "customer/edit";
    }

    @PostMapping("/update/{id}")
    public String update(@PathVariable("id") long id, @Valid Customer cust, 
      BindingResult result, Model model) {
        if (result.hasErrors()) {
            cust.setId(id);
            return "customer/edit";
        }
        customerRepository.save(cust);
        return "redirect:/customer/index";
    }
        
    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") long id, Model model) {
        Customer cust = customerRepository.findById(id)
          .orElseThrow(() -> new IllegalArgumentException("Invalid customer Id:" + id));
        customerRepository.delete(cust);
        return "redirect:/customer/index";
    }

}
