package id.kimura.toko.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
    // import org.springframework.web.bind.annotation.GetMapping;
    // import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import id.kimura.toko.model.Product;
import id.kimura.toko.dao.ProductRepository;

@Controller
@RequestMapping("/product")
public class ProductController {

    @Autowired
    ProductRepository productRepository;

    @GetMapping("/create")
    public String create(Product product) {
        return "product/create";
    }

    @PostMapping("/save")
    public String save(@Valid Product product, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "product/create";
        }
        
        productRepository.save(product);
        return "redirect:/product/index";
    }

    @GetMapping("/index")
    public String index(Model model) {
        model.addAttribute("products", productRepository.findAll());
        return "product/index";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") long id, Model model) {
        Product product = productRepository.findById(id)
          .orElseThrow(() -> new IllegalArgumentException("Invalid product Id:" + id));
        
        model.addAttribute("product", product);
        return "product/edit";
    }

    @PostMapping("/update/{id}")
    public String update(@PathVariable("id") long id, @Valid Product product, 
      BindingResult result, Model model) {
        if (result.hasErrors()) {
            product.setId(id);
            return "product/edit";
        }
        productRepository.save(product);
        return "redirect:/product/index";
    }
        
    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") long id, Model model) {
        Product product = productRepository.findById(id)
          .orElseThrow(() -> new IllegalArgumentException("Invalid product Id:" + id));
        productRepository.delete(product);
        return "redirect:/product/index";
    }

}
