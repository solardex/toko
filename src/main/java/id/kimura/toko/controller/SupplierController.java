package id.kimura.toko.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.web.bind.annotation.GetMapping;
// import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import id.kimura.toko.model.Supplier;
import id.kimura.toko.dao.SupplierRepository;

@Controller
@RequestMapping("/supplier")
public class SupplierController {

    @Autowired
    SupplierRepository supplierRepository;

    @GetMapping("/create")
    public String create(Supplier supplier) {
        return "supplier/create";
    }

    @PostMapping("/save")
    public String save(@Valid Supplier supplier, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "supplier/create";
        }
        
        supplierRepository.save(supplier);
        return "redirect:/supplier/index";
    }

    @GetMapping("/index")
    public String index(Model model) {
        model.addAttribute("suppliers", supplierRepository.findAll());
        return "supplier/index";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, Model model) {
        Supplier supplier = supplierRepository.findById(id)
          .orElseThrow(() -> new IllegalArgumentException("Invalid supplier Id:" + id));
        
        model.addAttribute("supplier", supplier);
        return "supplier/edit";
    }

    @PostMapping("/update/{id}")
    public String update(@PathVariable("id") String id, @Valid Supplier supplier, 
      BindingResult result, Model model) {
        if (result.hasErrors()) {
            supplier.setId(id);
            return "supplier/edit";
        }
        supplierRepository.save(supplier);
        return "redirect:/supplier/index";
    }
        
    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") String id, Model model) {
        Supplier supplier = supplierRepository.findById(id)
          .orElseThrow(() -> new IllegalArgumentException("Invalid supplier Id:" + id));
        supplierRepository.delete(supplier);
        return "redirect:/supplier/index";
    }

}
