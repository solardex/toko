package id.kimura.toko.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.*;
// import org.springframework.web.bind.annotation.GetMapping;
// import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;


import org.springframework.data.domain.*;
// import org.springframework.data.domain.Sort;
// import org.springframework.data.domain.Page;
// import org.springframework.data.domain.PageRequest;

import id.kimura.toko.model.Brand;
import id.kimura.toko.dao.BrandRepository;
import id.kimura.toko.service.BrandService;

@Controller
@RequestMapping("/brand")
public class BrandController {

    @Autowired
    BrandRepository brandRepository;

    @Autowired
    BrandService brandService;

    @GetMapping("/create")
    public String create(Brand brand) {
        return "brand/create";
    }

    @PostMapping("/save")
    public String save(@Valid Brand brand, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "brand/create";
        }
        
        brandRepository.save(brand);
        return "redirect:/brand/index";
    }

    @GetMapping("/index")
    public String index(Model model) {
        model.addAttribute("brands", brandRepository.findAll(Sort.by("name")));
        return "brand/index";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") long id, Model model) {
        Brand brand = brandRepository.findById(id)
          .orElseThrow(() -> new IllegalArgumentException("Invalid brand Id:" + id));
        
        model.addAttribute("brand", brand);
        return "brand/edit";
    }

    @PostMapping("/update/{id}")
    public String update(@PathVariable("id") long id, @Valid Brand brand, 
      BindingResult result, Model model) {
        if (result.hasErrors()) {
            brand.setId(id);
            return "brand/edit";
        }
        brandRepository.save(brand);
        return "redirect:/brand/index";
    }
        
    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") long id, Model model) {
        Brand brand = brandRepository.findById(id)
          .orElseThrow(() -> new IllegalArgumentException("Invalid brand Id:" + id));
        brandRepository.delete(brand);
        return "redirect:/brand/index";
    }

    @RequestMapping(value = "/bpaging", method = RequestMethod.GET)
    public String bpaging(Model model, @RequestParam("page") Optional<Integer> page, @RequestParam("size") Optional<Integer> size) {
        final int currentPage = page.orElse(1);
        final int pageSize = size.orElse(5);

        Page<Brand> brandPage = brandService.findPaginated(PageRequest.of(currentPage - 1, pageSize));

        model.addAttribute("brandPage", brandPage);

        int totalPages = brandPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                .boxed()
                .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }

        // bpaging = baeldung paging
        // ref: https://www.baeldung.com/spring-thymeleaf-pagination
            // https://github.com/eugenp/tutorials/tree/master/spring-web-modules/spring-thymeleaf
        return "brand/bpaging.html";
    }


    // another paging
    // ref: https://github.com/jpenren/thymeleaf-spring-data-dialect
    @GetMapping("/jpaging")
    public String jpaging(ModelMap model, Pageable pageable){
        // example: http://localhost:8080/brand/jpaging?size=5
        model.addAttribute("brandPage", brandRepository.findAll(pageable));
        // jpaging = jpenren paging
        return "brand/jpaging.html";
    }

}
