package id.kimura.toko.controller;

import javax.validation.Valid;
import java.util.*;
    // import java.util.List;
    // import java.util.Map;
    // import java.util.HashMap;

import org.springframework.data.repository.query.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
// import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import id.kimura.toko.model.Supplier;
import id.kimura.toko.dao.SupplierRepository;

import id.kimura.toko.model.Brand;
import id.kimura.toko.dao.BrandRepository;

@RestController
@RequestMapping("/ac")
public class AutocompleteController {

    @Autowired
    SupplierRepository supplierRepository;

    @Autowired
    BrandRepository brandRepository;

    @GetMapping("brandObj/{id}")
    public ResponseEntity<Brand> brandObj(@PathVariable("id") Long id) {
        Brand brand = brandRepository.findById(id).orElse(null);
        if (brand == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(brand);
        }
    }

    @GetMapping("brand/{q}")
    public ResponseEntity<List> brand(@PathVariable("q") String q) {
        // return ResponseEntity.ok(brandRepository.ac(q));
        return ResponseEntity.ok(brandRepository.ja(q));
    }

    @GetMapping("brandJaCustom/{q}")
    public ResponseEntity<List> brandJaCustom(@PathVariable("q") String q) {
        return ResponseEntity.ok(brandRepository.jaCustom(q));
    }

    @GetMapping("brandS2")
    public ResponseEntity<List> brandS2(@Param("q") String q) {
        return ResponseEntity.ok(brandRepository.s2(q));
    }


}
