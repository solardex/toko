package id.kimura.toko.model;

// import javax.persistence.Entity;
// import javax.persistence.GeneratedValue;
// import javax.persistence.GenerationType;
// import javax.persistence.Id;
// import javax.persistence.Table;
// import javax.persistence.Column;
// import javax.persistence.UniqueConstraint;
// import javax.persistence.OneToOne;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

import lombok.Data;
@Data 
@Entity
@Table(
	uniqueConstraints = { 
		@UniqueConstraint(
			name = "UK_brand_code", columnNames = { "code" }
		)
		, @UniqueConstraint(
			name = "UK_brand_name", columnNames = { "name" }
		)
	}
)
public class Brand {
	// example of AUTO Generation numeric / sequence
    // @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    @GeneratedValue
    private long id;
    
    @NotBlank(message = "Name is mandatory")
    private String name;
    
    @Column(unique = true, length=35)
    @NotBlank(message = "Code is mandatory")
    private String code;

    // @OneToMany(mappedBy="brand")
    // private List<Product> products;
}
