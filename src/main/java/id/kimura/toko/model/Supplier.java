package id.kimura.toko.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
// import java.util.UUID;

import lombok.Data;
@Data 
@Entity
@Table(
	uniqueConstraints = { 
		@UniqueConstraint(
			name = "UK_supplier_code", columnNames = { "code" }
		)
	}
)
public class Supplier {

	// example of AUTO Generation UUID 
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(length=72)
    private String id; // it should be 36 chars length

    @Column(unique = true, length=35)
    @NotBlank(message = "Code is mandatory")
    private String code;

    @Column(length=100)
    @NotBlank(message = "Name is mandatory")
    private String name; // supplier company name

    @Column(length=100)
    @NotBlank(message = "PIC is mandatory")
    private String pic; // person in charge
    
	
}
