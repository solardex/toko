package id.kimura.toko.model;

// import javax.persistence.Entity;
// import javax.persistence.GeneratedValue;
// import javax.persistence.GenerationType;
// import javax.persistence.Id;
// import javax.persistence.Table;
// import javax.persistence.Column;
// import javax.persistence.UniqueConstraint;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

import lombok.Data;

@Data 
@Entity
@Table(name="product"
	, uniqueConstraints = { 
		@UniqueConstraint(name = "UK_product_code", columnNames = { "code" })
	}
)
public class Product {
    // @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    @GeneratedValue(generator = "sequence-generator")
    @GenericGenerator(
		name = "sequence-generator"
		, strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator"
		, parameters = {
			@Parameter(name = "sequence_name", value = "product_seq")
			, @Parameter(name = "initial_value", value = "1")
			, @Parameter(name = "increment_size", value = "1")
		}
    )
    private long id;
    
    @NotBlank(message = "Name is mandatory")
    private String name;
    
    @Column(unique = true, length=35)
    @NotBlank(message = "Code is mandatory")
    private String code;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date releaseDate;

    @ManyToOne
    @JoinColumn(name="brand_id")
    private Brand brand;
	
}
