package id.kimura.toko.model;

import javax.persistence.*;
// import javax.persistence.PrePersist;

import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
// import java.util.UUID;

import lombok.Data;
@Data 
@Entity
@Table(
	uniqueConstraints = { 
		@UniqueConstraint(
			name = "UK_customer_code", columnNames = { "code" }
		)
	}
)
public class Customer {

    @Id
    @GeneratedValue(generator = "sequence-generator")
    @GenericGenerator(
        name = "sequence-generator"
        , strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator"
        , parameters = {
            @Parameter(name = "sequence_name", value = "customer_seq")
            , @Parameter(name = "initial_value", value = "1")
            , @Parameter(name = "increment_size", value = "1")
        }
    )
    private long id;

    @Column(unique = true, length=35)
    @NotBlank(message = "Code is mandatory")
    private String code;

    @Column(length=100)
    @NotBlank(message = "Name is mandatory")
    private String name; // customer's company name

    @Column(length=100)
    @NotBlank(message = "Contact person is mandatory")
    private String cp; // contact person
    
    @Column(length=100)
    @NotBlank(message = "Contact person's phone is mandatory")
    private String cpPhone;

    private Date created;

    @PrePersist
    public void beforeInsert() {
        created = new Date();
    }

	
}
