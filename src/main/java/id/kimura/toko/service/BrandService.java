package id.kimura.toko.service;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import id.kimura.toko.model.Brand;
import id.kimura.toko.dao.BrandRepository;

@Service
public class BrandService {

    @Autowired
    BrandRepository brandRepository;

    // final private List<Brand> brands = brandRepository.findAll();

    public Page<Brand> findPaginated(Pageable pageable) {
        // optimize me
        List<Brand> brands = brandRepository.findAll();

        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Brand> list;

        if (brands.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, brands.size());
            list = brands.subList(startItem, toIndex);
        }

        Page<Brand> brandPage = new PageImpl<Brand>(list, PageRequest.of(currentPage, pageSize), brands.size());

        return brandPage;

    }
}
