package id.kimura.toko;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.thymeleaf.dialect.springdata.SpringDataDialect;

@SpringBootApplication
public class TokoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TokoApplication.class, args);
	}

	// paging jpenren
    @Bean
    public SpringDataDialect springDataDialect() {
        return new SpringDataDialect();
    }
}
