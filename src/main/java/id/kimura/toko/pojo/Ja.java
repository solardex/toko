package id.kimura.toko.pojo;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

import lombok.Data;
@Data 
public class Ja {
	// Ja = Jquery Autocomplete
    private Long id;
    private String value;
    private String label;

    public Ja(Long id, String value, String label) {
    	this.id = id;
    	this.value = value;
    	this.label = label;
    }
}
