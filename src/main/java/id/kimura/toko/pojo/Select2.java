package id.kimura.toko.pojo;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

import lombok.Data;
@Data 
public class Select2 {
    private Long id;
    private String text;

    public Select2(Long id, String text) {
    	this.id = id;
    	this.text = text;
    }
}
