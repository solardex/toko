package id.kimura.toko.pojo;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

import lombok.Data;
@Data 
public class JaCustom extends Ja {
	// Ja = Jquery Autocomplete
    private String name; // column tambahan, misalnya field 'name'

    public JaCustom(Long id, String value, String label, String name) {
        super(id, value, label);
        this.name = name;
    }
}
